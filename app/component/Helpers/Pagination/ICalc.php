<?php

namespace Rudolf\Component\Helpers\Pagination;

interface ICalc
{
    public function nav();

    public function getAllPages();
}
