<?php

namespace Rudolf\Component\Helpers\Pagination;

interface IItem
{
    public function setData($item);
}
